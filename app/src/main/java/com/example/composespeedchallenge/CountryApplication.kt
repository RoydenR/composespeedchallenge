package com.example.composespeedchallenge

import android.app.Application
import com.example.composespeedchallenge.module.koinModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class CountryApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@CountryApplication)
            modules(koinModule)
        }
    }
}