package com.example.composespeedchallenge

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Color.Companion
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.composespeedchallenge.response.CountryInfoDTO
import com.example.composespeedchallenge.response.CountryInfoDTOItem
import com.example.composespeedchallenge.state.CountryState
import com.example.composespeedchallenge.ui.theme.ComposeSpeedChallengeTheme
import com.example.composespeedchallenge.viewModel.CountryViewModel
import org.koin.androidx.compose.getViewModel


class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val countryViewModel = getViewModel<CountryViewModel>()
            countryViewModel.countryList()
            val countryState: CountryState by countryViewModel.country.collectAsState()

            ComposeSpeedChallengeTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CountryList(
                        countryState = countryState.countryList
                    )
                }
            }
        }
    }
}

@Composable
fun CountryList(
    countryState: List<CountryInfoDTOItem>
) {
    LazyColumn(
        modifier = Modifier
            .fillMaxHeight()
            .fillMaxWidth()
            .padding(16.dp),
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        items(countryState) { country: CountryInfoDTOItem ->
            Card(
                modifier = Modifier
                    .padding(16.dp)
                    .fillMaxWidth(),
                shape = RoundedCornerShape(24.dp),
            ) {
                Column {
                    Row {
                        Text(
                            text = country.name,
                            fontSize = 24.sp,
                            color = Color.Black
                        )
                        Text(
                            text = country.code,
                            fontSize = 24.sp,
                            color = Color.Black
                        )
                    }
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun CountryListPreview() {
    ComposeSpeedChallengeTheme {
        CountryList(
            countryState = emptyList()
        )
    }
}