package com.example.composespeedchallenge.domain

import com.example.composespeedchallenge.response.CountryInfoDTO
import com.example.composespeedchallenge.response.CountryInfoDTOItem
import com.example.composespeedchallenge.service.CountryRepo
import java.lang.Exception
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class CountryListUseCase(private val countryRepo: CountryRepo) {
    suspend operator fun invoke(): Result<List<CountryInfoDTOItem>> = withContext(Dispatchers.IO) {
        return@withContext try {
            val response: List<CountryInfoDTOItem> = countryRepo.getCountryList()
            val displayCountries: List<CountryInfoDTOItem> = response
            Result.success(displayCountries)
        } catch (ex: Exception){
            Result.failure(ex)
        }
    }
}