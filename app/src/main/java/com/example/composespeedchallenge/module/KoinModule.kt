package com.example.composespeedchallenge.module

import com.example.composespeedchallenge.domain.CountryListUseCase
import com.example.composespeedchallenge.service.CountryRepo
import com.example.composespeedchallenge.service.CountryService
import com.example.composespeedchallenge.service.CountryService.Companion
import com.example.composespeedchallenge.service.CountryServiceImpl
import com.example.composespeedchallenge.viewModel.CountryViewModel
import io.ktor.client.HttpClient
import org.koin.androidx.compose.get
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.scope.get
import org.koin.dsl.module


val koinModule = module {
    single { CountryService.createCountryService() }
    single { CountryRepo(get()) }
    single { CountryListUseCase(get()) }
    single { CountryViewModel(get()) }
}
