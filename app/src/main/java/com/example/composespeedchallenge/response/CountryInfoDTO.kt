package com.example.composespeedchallenge.response

import kotlinx.serialization.Serializable

@Serializable
data class CountryInfoDTO(
    val countryList: List<CountryInfoDTOItem> = emptyList()
)