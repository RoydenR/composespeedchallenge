package com.example.composespeedchallenge.response


import com.google.gson.annotations.SerializedName
import kotlinx.serialization.Serializable

@Serializable
data class Language(
    @SerializedName("code")
    val code: String = "",
    @SerializedName("iso639_2")
    val iso6392: String = "",
    @SerializedName("name")
    val name: String = "",
    @SerializedName("nativeName")
    val nativeName: String = ""
)