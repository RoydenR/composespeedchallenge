package com.example.composespeedchallenge.service

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class CountryRepo(private val countryService: CountryService) {

    suspend fun getCountryList() = withContext(Dispatchers.IO) {
        countryService.getCountries()
    }
}
