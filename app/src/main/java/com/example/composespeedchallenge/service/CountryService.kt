package com.example.composespeedchallenge.service

import com.example.composespeedchallenge.response.CountryInfoDTO
import com.example.composespeedchallenge.response.CountryInfoDTOItem
import io.ktor.client.HttpClient

interface CountryService {
    suspend fun getCountries(): List<CountryInfoDTOItem>

    companion object {
        fun createCountryService(): CountryService {
            return CountryServiceImpl()
        }
    }
}