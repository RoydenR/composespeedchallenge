package com.example.composespeedchallenge.service

import com.example.composespeedchallenge.response.CountryInfoDTO
import com.example.composespeedchallenge.response.CountryInfoDTOItem
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.engine.android.Android
import io.ktor.client.request.get
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json

class CountryServiceImpl : CountryService {
    override suspend fun getCountries(): List<CountryInfoDTOItem> {
       val client =  HttpClient(Android)
        val json = Json {
            ignoreUnknownKeys = true
            isLenient = true
        }
        return try {
            val getResponse = client.get(CountryRoutes.COUNTRY).body<String>()
            val decodeResponse: List<CountryInfoDTOItem> = json.decodeFromString(getResponse)
            decodeResponse
        } catch (ex: Exception) {
            ex.localizedMessage
            emptyList()
        }
    }
}

object CountryRoutes {
    private const val BASE_URL = "https://gist.githubusercontent.com"
    const val COUNTRY = "$BASE_URL/peymano-wmt/" +
            "32dcb892b06648910ddd40406e37fdab/raw/" +
            "db25946fd77c5873b0303b858e861ce724e0dcd0/" +
            "countries.json"
}
