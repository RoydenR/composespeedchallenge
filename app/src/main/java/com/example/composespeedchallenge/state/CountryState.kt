package com.example.composespeedchallenge.state

import com.example.composespeedchallenge.response.CountryInfoDTO
import com.example.composespeedchallenge.response.CountryInfoDTOItem

data class CountryState(
    val isLoading: Boolean = false,
    val countryList: List<CountryInfoDTOItem> = emptyList(),
    val error: String? = null
)
