package com.example.composespeedchallenge.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.composespeedchallenge.domain.CountryListUseCase
import com.example.composespeedchallenge.response.CountryInfoDTO
import com.example.composespeedchallenge.response.CountryInfoDTOItem
import com.example.composespeedchallenge.state.CountryState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class CountryViewModel(private val countryListUseCase: CountryListUseCase) : ViewModel() {

    private val _country = MutableStateFlow(CountryState())
    val country = _country.asStateFlow()

    fun countryList() {
        viewModelScope.launch(Dispatchers.IO) {
            _country.value = _country.value.copy(isLoading = true)
            val result = countryListUseCase.invoke()
            if (result.isSuccess) {
                val displayCountries: List<CountryInfoDTOItem> = result.getOrThrow()
                _country.value = _country.value.copy(countryList = displayCountries)
            } else {
                _country.value = _country.value.copy(error = Error().localizedMessage)
            }
            _country.value = _country.value.copy(isLoading = false)
        }
    }
}